module.exports = {
    401: 'Unauthorized!',
    403: 'Access denied!',
    404: 'Route not found!',
    500: 'Server error!'
}
