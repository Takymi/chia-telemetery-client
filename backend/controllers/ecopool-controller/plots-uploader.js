const moment = require('moment');
const getPlots = require('../chia-controller/get-plots');
const addPlots = require('./add-plots');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('ecopool-controller:plots-uploader');
const state = require('../../state');

const timeout = 1000 * 60 * 5; // 5 минут
let failedTimeout = null;
let isStarted = false;
let startTimeSend = 0;
let endTimeSend = 0;
let timeIsOver = false;

const sendPlots = async () => {
    const allPlots = await getPlots();

    if (!allPlots) {
        failedTimeout = setTimeout(() => {
            debugLog('The getPlots function returned false, the process of sending data by plots to the server will be restarted in 10 seconds');
            sendPlots();
            clearTimeout(failedTimeout);
        }, 10000);
        return false;
    }

    state.totalPlotsCount = allPlots.length;
    startTimeSend = Date.now();
    debugLog(`Start time of sending data on plots to the server: ${moment(startTimeSend).format('hh:mm:ss:SSS')}`);

    try {
        await addPlots(allPlots);
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Function addPlots failed',
            error
        }));
    }

    endTimeSend = Date.now();
    debugLog(`End time of sending data on plots to the server: ${moment(endTimeSend).format('hh:mm:ss:SSS')}`);

    const timeDiff = endTimeSend - startTimeSend;

    debugLog(`Difference: ${timeDiff}`);
    timeIsOver = timeDiff >= timeout;

    if (timeIsOver) {
        debugLog('The waiting time of 5 minutes has expired, we start re-sending the data over the plots to the server immediately');
        sendPlots();
    } else {
        const timeLeft = timeout - timeDiff;

        debugLog(`The waiting time of 5 minutes has not expired, we are still waiting for ${timeLeft}, and we are starting to re-send data by plots to the server`);

        setTimeout(() => {
            debugLog('Re-sending of data by plots to the server started');
            sendPlots();
        }, timeout - timeDiff);
    }
};

module.exports = {
    start: () => {
        if (isStarted) {
            debugLog('Sending data by plots to the server is already running');
            return false;
        }

        sendPlots();
        isStarted = true;
        debugLog('Sending data on plots to the server started');
    },
    stop: () => {
        isStarted = false;
        debugLog('Sending data on plots to the server stopped');
    }
};
