const config = require('config');
const IOClient = require('socket.io-client');
const io = IOClient(config.pool.host);

io.on('connect', () => {
    const setUser = require('./set-user');
    setUser.mainSocket(config.currentUser);
});

module.exports = io;
