const ECO_POOL_API = require('../../constants/eco-pool-api');
const GuiController = require('../../controllers/gui-controller');
const apiRequest = require('./api-request');
const stop = require('./stop');
const getLogs = require('../../utils/get-logs');
const { debugLog } = getLogs('ecopool-controller:logout');

module.exports = () => {
    apiRequest({
        method: 'get',
        url: ECO_POOL_API.logout
    });

    GuiController.stop();
    stop();
    debugLog('User is successfully logged out');

    return { success: true };
};
