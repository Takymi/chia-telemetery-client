const localConfig = require('./local-config');

module.exports = () => {
    return Boolean(localConfig.get('currentUser'));
};
