const localConfig = require('./local-config');
const userIsLogged = require('./user-is-logged');
const createPlot = require('./create-plot');
const createPlots = require('./create-plots');
const removePlot = require('./remove-plot');
const stop = require('./stop');

module.exports = {
    localConfig,
    userIsLogged,
    createPlot,
    createPlots,
    removePlot,
    stop
};
