const config = require('config');
const { readdirSync } = require('fs');
const semver = require('semver');
const {homedir} = require('os');

module.exports = () => {
    if (config.chiaCliPath) {
        return config.chiaCliPath;
    }

    if (process.platform === 'win32') {
        const sourcePath = `${homedir()}\\AppData\\Local\\chia-blockchain\\`;

        const semverDirs = readdirSync(sourcePath, { withFileTypes: true })
            .filter(directory => {
                return directory.isDirectory() && semver.valid(directory.name.replace('app-', ''));
            })
            .map(directory => {
                return directory.name.replace('app-', '');
            });

        if (!semverDirs.length) {
            return undefined;
        } else {
            const bestVersion = semverDirs.sort(semver.compare).reverse()[0];
            return `"${homedir()}\\AppData\\Local\\chia-blockchain\\app-${bestVersion}\\resources\\app.asar.unpacked\\daemon\\chia.exe"`;
        }
    } else {
        return '~/chia-blockchain';
    }
};
