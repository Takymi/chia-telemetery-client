const startHarvester = require('./start-harvester');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('chia-controller:restart-harvester');
const state = require('../../state');

module.exports = () => {
    debugLog('Harvester restart started');
    state.harvesterIsWorking = false;

    const restartHarvesterStatus = startHarvester();

    if (restartHarvesterStatus) {
        debugLog('Harvester restart completed successfully');
    } else {
        debugError('An error occurred when restarting the harvester');
    }

    return restartHarvesterStatus;
};
