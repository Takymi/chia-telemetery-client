const path = require('path');
const { homedir } = require('os');
const fs = require('fs');
const getHash = require('../../utils/get-hash');
const pathToCert = path.resolve(homedir(), './.chia/mainnet/config/ssl/harvester/private_harvester.crt');
const getLogs = require('../../utils/get-logs');
const { debugError } = getLogs('chia-controller:get-harvester-id');

module.exports = () => {
    try {
        const certificate = fs.readFileSync(pathToCert).toString()
            .split('\n')
            .filter(line => !line.includes('-----'))
            .map(line => line.trim() )
            .join('');

        return getHash(certificate, 'base64', 'hex');
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Can`t read harvester certificate',
            error
        }));
        return null;
    }
};
