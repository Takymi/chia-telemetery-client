const callCLIPromise = require('./call-cli-promise');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        commandPayload: {
            configure: null,
            '--set-log-level': null,
            DEBUG: null
        },
        commandForStartHarvester: true
    });

    return checkCommand.status;
};
