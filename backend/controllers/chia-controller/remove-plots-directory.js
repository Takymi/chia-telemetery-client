const HarvesterInstance = require('./harvester-instance');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('chia-controller:remove-plots-directory');
const state = require('../../state');

module.exports = async (directory) => {
    if (!directory) {
        debugError('Directory with plots not specified');
        return false;
    }

    if (!state.harvesterIsWorking) {
        debugWarn('Harvester not started. Command could not be executed');
        return false;
    }

    const { success } = await HarvesterInstance.removePlotDirectory(directory);

    if (success) {
        debugLog(`Directory ${directory} with plots deleted successfully`);
    } else {
        debugError(`Failed to delete directory ${directory} with plots`);
    }

    return success;
};
