const callCLIPromise = require('./call-cli-promise');
const state = require('../../state');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        commandPayload: {
            stop: null,
            all: null,
            '-d': null
        },
        commandForStartHarvester: true
    });

    if (checkCommand.status) {
        state.harvesterIsWorking = false;
    }

    return checkCommand.status;
};
