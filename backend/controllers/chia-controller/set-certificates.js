const path = require('path');
const callCLIPromise = require('./call-cli-promise');

module.exports = async () => {
    const certificatesPath = path.join(process.cwd(), '/pool-certificates');
    const checkCommand = await callCLIPromise({
        commandPayload: {
            init: null,
            '-c': null,
            [certificatesPath]: null
        },
        commandForStartHarvester: true
    });

    return checkCommand.status;
};
