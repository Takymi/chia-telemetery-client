const discoverCLI = require('./discover-cli');
const callCLIPromise = require('./call-cli-promise');
const GuiController = require('../gui-controller');

module.exports = async (customPath = null) => {
    const CLIPath = customPath ? customPath : discoverCLI();
    const checkCLICommand = await callCLIPromise({
        commandPayload: {
            '-h': null
        },
        customCLIPath: CLIPath,
        commandForStartHarvester: true
    });

    if (checkCLICommand.status) {
        GuiController.localConfig.set('chiaCliPath', CLIPath);
    }

    return checkCLICommand.status;
};
