const fs = require('fs');
const path = require('path');
const { homedir } = require('os');
const Tail = require('tail').Tail;
const pathToLogFile = path.join(homedir(), '.chia', 'mainnet', 'log', 'debug.log');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('chia-controller:log-watcher');

const lastOnline = require('./log-watcher/last-online');
const checkPlots = require('./log-watcher/check-plots');
const harvesterTelemetry = require('./log-watcher/harvester-telemetry');

let sockets = [];
let starting = false;
let TailInstance;

const updateSockets = ($socket) => {
    sockets.push($socket);

    return sockets.filter(socket => {
        return socket.connected;
    });
};
const start = (socket) => {
    if (socket) {
        sockets = updateSockets(socket);
    }

    if (sockets.length) {
        debugLog('Sockets were successfully forwarded to the log watcher');
    } else {
        debugWarn('No sockets were forwarded to the log watcher. No data will be sent to the client !!!');
    }

    if (starting) {
        debugWarn('Reading chia logs is already running, you can only restart');
        return false;
    }

    TailInstance = new Tail(pathToLogFile);

    TailInstance.on('line', data => {
        lastOnline(data, sockets);
        checkPlots(data, sockets);
        harvesterTelemetry(data, sockets);
    });

    TailInstance.on('error', error => {
        debugError(JSON.stringify({
            message: 'An error occured while reading chia logs',
            error
        }));
        setTimeout(restart, 3000);
    });

    starting = true;
    debugLog('Reading chia logs started');
};
const stop = () => {
    if (!fs.existsSync(pathToLogFile)) {
        return false;
    }

    TailInstance.unwatch();
    starting = false;
    debugLog('Reading chia logs stopped');
};
const restart = () => {
    if (!fs.existsSync(pathToLogFile)) {
        setTimeout(restart, 3000);
        return false;
    }

    stop();
    start();
};

module.exports = {
    start,
    restart,
    stop
};
