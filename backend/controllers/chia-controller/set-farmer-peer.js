const config = require('config');
const callCLIPromise = require('./call-cli-promise');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        commandPayload: {
            configure: null,
            '--set-farmer-peer': null,
            [`${config.pool.ip}:8447`]: null
        },
        commandForStartHarvester: true
    });

    return checkCommand.status;
};
