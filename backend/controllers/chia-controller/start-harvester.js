const callCLIPromise = require('./call-cli-promise');
const state = require('../../state');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        commandPayload: {
            start: null,
            harvester: null,
            '-r': null
        },
        commandForStartHarvester: true
    });

    if (checkCommand.status) {
        state.harvesterIsWorking = true;
    }

    return checkCommand.status;
};
