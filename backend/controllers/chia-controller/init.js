const checkCLI = require('./check-cli');
const stopAllProcess = require('./stop-all-process');
const setCertificates = require('./set-certificates');
const setUpnp = require('./set-upnp');
const setFarmerPeer = require('./set-farmer-peer');
const setLogToDebug = require('./set-log-to-debug');
const startHarvester = require('./start-harvester');
const EcoPoolController = require('../ecopool-controller');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('chia-controller:init');
const state = require('../../state');

module.exports = async () => {
    if (state.harvesterIsWorking) {
        debugWarn('The harvester is already running');
        EcoPoolController.plotsUploader.start();
        return false;
    }

    const checkCLIStatus = await checkCLI();

    if (!checkCLIStatus) {
        debugError('Path to chia-blockchain not found');
        return false;
    }

    debugLog('Path to chia-blockchain found successfully');

    const stopAllProcessStatus = await stopAllProcess();

    if (!stopAllProcessStatus) {
        debugWarn('Failed to stop all chia processes');
    } else {
        debugLog('All chia processes stopped successfully');
    }

    const setCertificatesStatus = await setCertificates();

    if (!setCertificatesStatus) {
        debugError('Failed to install pool certificates');
        return false;
    }

    debugLog('Pool certificates installed successfully');

    const setUpnpnStatus = await setUpnp();

    if (!setUpnpnStatus) {
        debugError('Failed to set upnp = false');
        return false;
    }

    debugLog('Successfully set upnp = false');

    const setFarmerPeerStatus = await setFarmerPeer();

    if (!setFarmerPeerStatus) {
        debugError('Failed to set pool ip');
        return false;
    }

    debugLog('Pool ip set successfully');

    const setLogToDebugStatus = await setLogToDebug();

    if (!setLogToDebugStatus) {
        debugError('Failed to install chia logs in DEBUG mode');
        return false;
    }

    debugLog('Chia logs installed successfully in DEBUG mode');

    const startHarvesterStatus = await startHarvester();

    if (!startHarvesterStatus) {
        debugError('Failed to start harvester');
        return false;
    }

    debugLog('Harvester successfully launched');

    setTimeout(() => {
        EcoPoolController.plotsUploader.start();
    }, 10000);

    return true;
};
