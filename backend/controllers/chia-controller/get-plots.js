const getPlotMemo = require('../../utils/get-plot-memo');
const getPreparedPlotDataForServer = require('../../utils/get-prepared-plot-data-for-server');
const HarvesterInstance = require('./harvester-instance');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('chia-controller:get-plots');
const state = require('../../state');

module.exports = async () => {
    if (!state.harvesterIsWorking) {
        debugWarn('Harvester not started. Command could not be executed');
        return false;
    }

    const plots = await HarvesterInstance.getPlots();

    if (!plots.success) {
        debugError('getPlots method by HarvesterInstance failed');
        return [];
    }

    const plotsData = plots.plots;

    if (!plotsData.length) {
        debugLog('User has no valid plots');
        return [];
    }

    const preparedPlotsData = [];

    for (const plotData of plotsData) {
        preparedPlotsData.push({
            ...getPreparedPlotDataForServer(plotData),
            memo: await getPlotMemo(plotData.filename)
        });
    }

    debugWarn('Data on valid plots successfully collected');

    return preparedPlotsData;
};
