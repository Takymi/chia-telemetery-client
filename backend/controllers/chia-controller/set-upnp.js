const callCLIPromise = require('./call-cli-promise');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        commandPayload: {
            configure: null,
            '--enable-upnp': null,
            false: null
        },
        commandForStartHarvester: true
    });

    return checkCommand.status;
};
