module.exports = {
    harvesterIsWorking: false,
    token: null,
    queues: {},
    histories: {
        checkPlots: [],
        farmingPlots: [],
        proofs: []
    },
    totalPlotsCount: 0
};
