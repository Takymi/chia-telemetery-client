const config = require('config');
const express = require('express');
const app = express();
const http = require('http');
const path = require('path');
const cookieParser = require('cookie-parser');
const fronted = require('../routes/0040.0000-frontend');
const getLogs = require('../utils/get-logs');
const { debugLog, debugError } = getLogs('gui-www');

const normalizePort = (val) => {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
};
const onListening = () => {
    const addr = server.address();
    const bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;

    console.log(`Your server started on http://localhost:${config.client.port}`);
    debugLog(`Listening on ${bind}`);
};
const onError = (error) => {
    if (error.syscall !== 'listen') {
        throw error;
    }

    const bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    switch (error.code) {
        case 'EACCES':
            debugError(`${bind} requires elevated privileges`);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            debugError(`${bind} is already in use`);
            process.exit(1);
            break;
        default:
            throw error;
    }
};
const port = normalizePort(config.client.port || 3401);
const server = http.createServer(app);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/', express.static(path.resolve(process.cwd(), 'dist')));
app.use(fronted.router);

server.listen(port, `0.0.0.0`);
server.on('error', onError);
server.on('listening', onListening);
