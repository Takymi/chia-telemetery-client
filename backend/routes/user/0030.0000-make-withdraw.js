const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const EcoPoolController = require('../../controllers/ecopool-controller');

router.post('/api/user/make-withdraw', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const {
        body: {
            amount,
            xchAddress,
            password,
            timestamp
        }
    } = req;

    if (isNaN(+amount)) {
        return res.send({
            success: false,
            answer: {
                error: 'Amount is not a number!'
            }
        });
    }

    const requestData = await EcoPoolController.makeWithdraw({
        address: xchAddress,
        amount,
        timestamp
    }, password);

    const { success, answer } = requestData;

    if (!success) {
        return res.send(requestData);
    }

    return res.send({
        success: true,
        answer: {
            userBalance: answer
        }
    });
});

module.exports = { router };
