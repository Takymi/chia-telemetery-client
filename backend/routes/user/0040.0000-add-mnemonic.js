const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const EcoPoolController = require('../../controllers/ecopool-controller');

router.post('/api/user/add-mnemonic', async (req, res, next) => {
    const { body: { mnemonic } } = req;

    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    if (!mnemonic) {
        return { success: false };
    }

    const addMnemonic = await EcoPoolController.addMnemonic(mnemonic);

    if (addMnemonic && !addMnemonic.success) {
        return res.send({ success: false });
    } else {
        return res.send();
    }
});

module.exports = { router };
