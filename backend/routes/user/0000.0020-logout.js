const router = require('express').Router();
const EcoPoolController = require('../../controllers/ecopool-controller');

router.get('/api/logout', async (req, res) => {
    return res.send(await EcoPoolController.userLogout());
});

module.exports = { router };
