const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const ChiaController = require('../../controllers/chia-controller');
const state = require('../../state');

router.get('/api/harvester/get-plots-directories', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    if (!state.harvesterIsWorking) {
        return res.send({
            success: false,
            harvesterNotWorking: true
        });
    }

    const { success, directories } = await ChiaController.getPlotsDirectories();

    return res.send({
        success,
        answer: { directories }
    });
});

module.exports = { router };
