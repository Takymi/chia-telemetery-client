const app = require('../../app');
const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
let socket = null;

router.post('/api/harvester/remove-plot', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const { body: { queueName, plotId } } = req;
    const removePlotStatus = GuiController.removePlot(queueName, plotId, socket);

    return res.send({
        success: removePlotStatus
    });
});

module.exports = {
    needSocket: true,
    setSocket: (socketInstance) => {
        socket = socketInstance;
    },
    router
};
