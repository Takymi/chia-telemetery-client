const router = require('express').Router();
const fs = require('fs');
const path = require('path');
const FRONTEND_PATHS = [
    '/login',
    '/registration',
    '/dashboard',
    '/wallet',
    '/plotting',
    '/plots-directories',
    '/plots/create',
    '/farm',
    '/mnemonics',
    '/request_chia_path'
];
const indexContent = fs.readFileSync(path.resolve(process.cwd(), 'dist', 'index.html'));

router.get(FRONTEND_PATHS, (req, res) => {
    res.set('Content-Type', 'text/html');
    res.send(indexContent);
});

module.exports = { router };
