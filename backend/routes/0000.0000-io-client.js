const ChiaController = require('../controllers/chia-controller');
const getLogs = require('../utils/get-logs');
const { debugLog } = getLogs('routes:io-client');

module.exports = {
    init (expressApp) {
        const io = expressApp.io;

        const applyIoRoutes = require('../io-routes');

        io.on('connection', (socket) => {
            debugLog('Sockets successfully connected');

            applyIoRoutes(socket);
            ChiaController.logWatcher.start(socket);
        });
    }
};
