const fs = require('fs');
const routerModules = fs.readdirSync(`${__dirname}/gui/`)
    .sort((a, b) => {
        if (a < b) {
            return -1;
        }

        if (a > b) {
            return 1;
        }

        return 0;
    })
    .map(fileName => {
        return require(`./gui/${fileName}`);
    });

module.exports = {
    routes: routerModules
};
