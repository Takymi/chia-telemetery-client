const fs = require('fs');

const routerModules = fs.readdirSync(__dirname)
    .filter(fileName => {
        return fileName !== 'index.js';
    })
    .sort((a,b) => {
        if (a < b) {
            return -1;
        }

        if (a > b) {
            return 1;
        }

        return 0;
    })
    .map(fileName => {
        return require(`./${fileName}`);
    });

module.exports = (socketInstance) => {
    for (const routerModule of routerModules) {
        if (routerModule.init) {
            routerModule.init(socketInstance);
        }
    }
};
