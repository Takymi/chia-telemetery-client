const getLogs = require('../utils/get-logs');
const { debugLog } = getLogs('io-routes:new-mnemonics');

module.exports = {
    init (socket) {
        socket.on('new-mnemonics', data => {
            const resp = {
                success: Boolean(data),
                answer: data ? data : 'error'
            };

            debugLog('Resend mnemonic from server to frontend');
            socket.emit('new-mnemonic', resp);
        });
    }
};
