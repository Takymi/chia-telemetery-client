const state = require('../state');

module.exports = (queueName, plotId) => {
    return state.queues[queueName].find(plotData => {
        return plotData.id === plotId;
    });
};
