module.exports = (error) => {
    if (!error) {
        return ''
    }

    const hasData = Boolean(error.config && error.config.data);

    if (hasData) {
        delete error.config.data;
    }

    return error;
};
