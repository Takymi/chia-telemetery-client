module.exports = (TiB) => {
    return TiB * Math.pow(1024, 4);
};
