import { io } from 'socket.io-client';

const socket = io(`${window.location.protocol}//${window.location.hostname}:${CONFIG.port}`, {
    reconnectionDelay: 1000,
});

socket.on('connect', () => {
    console.log('Sockets successfully connected');
});

export default socket;
