process.env['NODE_CONFIG_DIR'] = __dirname + '/../config/';
const config = require('config');
const ConfigWebpackPlugin = require('config-webpack');
const developClientPort = config && config.develop && config.develop.client && config.develop.client.port;

module.exports = {
  outputDir: '../dist',
  css: {
    loaderOptions: {
      sass: {
        additionalData: '@import "@/assets/scss/variables/index.scss";',
      }
    }
  },
  devServer: {
    port: developClientPort || 3402
  },
  configureWebpack: {
    plugins: [
      new ConfigWebpackPlugin()
    ]
  }
}
