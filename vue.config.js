const config = require('config');
const ConfigWebpackPlugin = require('config-webpack');
const developClientPort = config && config.client && config.client.port;

module.exports = {
  css: {
    loaderOptions: {
      sass: {
        additionalData: '@import "@/assets/scss/variables/index.scss";',
      }
    }
  },
  devServer: {
    port: developClientPort || 3401
  },
  configureWebpack: {
    plugins: [
      new ConfigWebpackPlugin()
    ]
  }
}
