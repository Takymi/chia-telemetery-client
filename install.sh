#!/bin/bash

#update packages
sudo apt update
sudo apt upgrade -y
sudo apt install figlet -y

#install chia
cd ~
rm -rf chia-blockchain
git clone https://github.com/Chia-Network/chia-blockchain.git -b latest --recurse-submodules
cd chia-blockchain
sh install.sh
sudo apt autoremove -y

#backup local.json
cd ~
FILE_local_json=./ecopool-client/config/local.json
if [ -f "$FILE_local_json" ]; then
	cp ./ecopool-client/config/local.json ./local.json.bak
fi

#install ecopool-client part 1
cd ~
rm -rf ecopool-client
git clone https://gitlab.com/ecopool-public/chia-telemetery-client.git ~/ecopool-client

FILE_local_json_bak=./local.json.bak
if [ -f "$FILE_local_json_bak" ]; then
	mv ./local.json.bak ./ecopool-client/config/local.json
else
	echo '{"chiaCliPath":"/home/'$USER'/chia-blockchain"}' > ./ecopool-client/config/local.json
fi

curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
sudo apt install nodejs -y

#init chia
cd ~/chia-blockchain
. ./activate
chia init
chia init -c ~/ecopool-client/pool-certificates

chia configure --set-farmer-peer 178.170.219.28:8447
chia configure --set-log-level DEBUG
chia configure --enable-upnp false

chia start harvester
echo "wait 10 sec."
sleep 10
chia stop all -d
chia stop all
deactivate

#install ecopool-client part 2
cd ~/ecopool-client
pm2 stop all
pm2 delete all

sudo npm i -g pm2
sudo npm i -g yarn
yarn install

pm2 start npm --name api -- run start
pm2 start npm --name frontend -- run serve
pm2 save

#set startup (pm2 startup)
sudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u $USER --hp /home/$USER

echo "wait 10 sec."
sleep 10
pm2 update
pm2 restart all

echo "wait 1 min."
sleep 60
echo ""
figlet CHIA ECOPOOL
echo ""
echo "==========================================================================="
echo "Ecopool client available on url http://localhost:3401 and http://$(hostname -I):3401"
echo "Клиент ecopool доступен по адресу http://localhost:3401 и http://$(hostname -I):3401"
echo "==========================================================================="
